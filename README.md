There is already a readme inside the ZendSkeletonApplication folder that details how to get the app setup on a server. For linux and windows I already had apache pretty much set up so all I really did was change the document root to the public folder inside the ZendSkeletonApplication folder.  

I used the zf2 skeleton app I was already tooling around with to complete the code challenge.

Once you are able to see the zf2 home page when navigating to localhost or whatever you call yours then you should be able to use the api at /mmmr

to simulate using the api at /mmmr I used curl to send a json object through with a post request