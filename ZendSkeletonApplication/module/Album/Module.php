<?php

namespace Album; //the classes within a module will have the namespace of the modules name.

use Album\Model\Album;
use Album\Model\AlbumTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module {

    //the ModuleManager will call getAutoloaderConfig() and getConfig() automatically for us
    public function getAutoloaderConfig() {
        //returns an array that is compatible with AutoloaderFactory
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(//add class map file to ClassMapAutoloader
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(//add this module's namespace to the StandardAutoloader
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    //in order to always use the same instance of AlbumTable we will use the ServiceManager to define how to create one.
    //this is done by creating getServiceConfig() which is automatically called by the ModuleManager and applied to the ServiceManager.
    public function getServiceConfig() {
        return array(
            'factories' => array(//we supply a factory that creates AlbumTable to configure ServiceManager
                'Album\Model\AlbumTable' => function($sm) { //uses ServiceManager to create an AlbumTableGateway to pass to AlbumTable
                    $tableGateway = $sm->get('AlbumTableGateway');
                    $table = new AlbumTable($tableGateway);
                    return $table;
                },
                'AlbumTableGateway' => function($sm) { //tell ServiceManager an AlbumTableGateway was created by getting a Zend\Db\Adapter\Adapter from the ServiceManager and using to create a TableGateway
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Album()); //the TableGateway is told to use an Album object whenever it creates a new result row
                    return new TableGateway('album', $dbAdapter, null, $resultSetPrototype); // the TableGateway classes use the prototype pattern for creation of resultsets and entities
                }
            ),
        );
    }
}
