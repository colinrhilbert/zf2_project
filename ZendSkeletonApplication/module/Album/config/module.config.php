<?php //the config information is passed to the relevant components by the ServiceManager
return array(
	'controllers' => array( //controllers section provides a list of all the controllers provided by the module
		'invokables' => array(
			'Album\Controller\Album' => 'Album\Controller\AlbumController',
		),
	),
	'router' => array(
		'routes' => array(
			'album' => array(
				'type' => 'segment', //type segment allows us to specify placeholders in the URL pattern(route) that will be mapped to named parameters in the matched route
				'options' => array(
					'route' => '/album[/][:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id' => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Album\Controller\Album',
						'action' => 'index',
					),
				),
			),
		),
	),
	'view_manager' => array(  //add out directory to the TemplatePathStack to allow it to find the view scripts stored in the view directory
		'template_path_stack' => array(
			'album' => __DIR__ . '/../view',
		),
	),
);