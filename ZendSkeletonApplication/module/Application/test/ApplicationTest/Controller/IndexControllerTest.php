<?php
namespace ApplicationTest\Controller;

use ApplicationTest\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Application\Controller\IndexController;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use PHPUnit_Framework_Testcase;

class IndexControllerTest extends \PHPUnit_Framework_Testcase
{
    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;
    
    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();
        $this->controller = new IndexController();
        $this->request = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'index'));
        $this->event = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);
        
        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);
    }
    
    public function testIndexActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'index');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    public function testMMMRActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'mmmr');
        $this->request->setMethod('POST');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }
    
    //this one is sent purposefully with GET method
    public function testMMMRActionCannotBeAccessed()
    {
        $this->routeMatch->setParam('action', 'mmmr');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }
    
    public function testCalculateMean()
    {
        $ic = $this->controller;
        $result = $ic->calculateMean(array(1,2,3,4,5));
        $expectedResult = (1 + 2 + 3 + 4 + 5) / 5;
        $this->assertEquals($expectedResult, $result);
    }
    
    public function testCalculateMode()
    {
        $ic = $this->controller;
        $result = $ic->calculateMode(array(1,2,2,4,5));
        $expectedResult = 2;
        $this->assertEquals($expectedResult, $result);
    }
    
    public function testCalculateMedian()
    {
        $ic = $this->controller;
        $result = $ic->calculateMedian(array(1,2,3,4,5));
        $expectedResult = 3;
        $this->assertEquals($expectedResult, $result);
    }
    
    public function testCalculateRange()
    {
        $ic = $this->controller;
        $result = $ic->calculateRange(array(1,2,2,4,5));
        $expectedResult = 4;
        $this->assertEquals($expectedResult, $result);
    }
}

