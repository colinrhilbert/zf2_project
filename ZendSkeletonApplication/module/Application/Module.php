<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $eventManager->attach('dispatch.error', array($this, 'onDispatchError'), 100); 
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
     public function onDispatchError(MvcEvent $e) 
        { 
            $error = $e->getError(); 
            if (!$error) { 
                // No error? nothing to do. 
                return; 
            } 

            $request = $e->getRequest(); 
            $headers = $request->getHeaders(); 
            if (!$headers->has('Accept')) { 
                // nothing to do; can't determine what we can accept 
                return; 
            } 

            $accept = $headers->get('Accept'); 
            if (!$accept->match('application/json')) { 
                // nothing to do; does not match JSON 
                return; 
            } 
            $response = $e->getResponse();
            $model = new JsonModel(array("error" => array("code" => $response->getStatusCode(), "message" => $response->getReasonPhrase())));

            $e->setResult($model); 
            $e->stopPropagation(); 
            return $model; 
        } 
    
}
