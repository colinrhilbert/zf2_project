<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }
    
    public function mmmrAction()
    {
        //$log = $this->getServiceLocator()->get('Zend\Log');
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->response->setStatusCode(404);
            return new JsonModel(array("error" => array("code" => 404,"message" => "Method GET not available on this endpoint")));
        } else {
            $data = $request->getContent();
            if(empty($data)){
                return new JsonModel(array("results" => ""));
            } else {
                $decodedJson = (json_decode($data, true));
                $numberArray = $decodedJson["numbers"];
                sort($numberArray);
                $mean = $this->calculateMean($numberArray);
                $mode = $this->calculateMode($numberArray);
                $median = $this->calculateMedian($numberArray);
                $range = $this->calculateRange($numberArray);
                return new JsonModel(array("results" => array(
                        "mean" => $mean, "mode" => $mode, "median" => $median, "range" => $range
                )));
            }
        }
    }
    
    //average
    public function calculateMean($numberArray)
    {
        $count = count($numberArray);
        $total = 0;
        foreach($numberArray as $value){
            $total += $value;
        }
        return ($total / $count);
    }
    
    public function calculateMode($numberArray)
    {
        $counts = array_count_values($numberArray);
        arsort($counts);
        $modes = array_keys($counts, current($counts), true);
        
        if(count($numberArray) === count($counts)){
            return false;
        }
        if(count($modes) === 1){
            return $modes[0];
        }
        return $modes;
    }
    
    public function calculateMedian($numberArray)
    {
        $count = count($numberArray);
        $middleValue = floor(($count - 1) / 2);
        if($count % 2){
            $median = $numberArray[$middleValue];
        } else {
            $median = (($numberArray[$middleValue] + $numberArray[$middleValue + 1]) / 2);
        }
        return $median;
    }
    
    public function calculateRange($numberArray)
    {
        return max($numberArray) - min($numberArray);
    }
}
